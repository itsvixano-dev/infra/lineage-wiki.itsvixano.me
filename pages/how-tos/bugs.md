---
sidebar: home_sidebar
title: How to submit a bug report
folder: how-tos
redirect_from: bugreport-howto.html
permalink: /how-to/bugreport/
tags:
 - how-to
---

## What not to report
  - Bugs in builds not downloaded from [our official portal](https://github.com/ItsVixano-releases)
  - Missing builds
  - Asking for installation help
  - Asking for device support
  - Feature requests

## Reporting a bug

We use GitHub Issues as our bug tracker. Before you submit a bug, please search to verify that someone else hasn't reported it already.

Please do not:
- Post "Me too" or "+1" messages on existing reports
- Comment your device as affected on a report for a different device - different devices have different maintainers, and different reasons for bugs to exist
- Ask if it is fixed already - if the report is still open it probably is still present!
- Ask for status updates - we can't provide them (no ETA!)
- Ping people/maintainers - most of them already check the tracker

We accept bugs on the following:

### Android bugs

These are bugs in LineageOS itself, e.g.
  - The fingerprint sensor on your device doesn't work.
  - Your phone crashes when you try to enable encryption.
  - WiFi calling doesn't work.

When adding a bug report, please do **not** use the email functionality, since we provide a template that needs to be filled out properly.

Please read the template carefully and make sure you are running the latest available version.
These boxes are parsed by a bot which will mark your issue as invalid if you do not fill them out properly.

Most of the requested information can be found on your device if you go to *Settings->About phone* and click on the `Android version` item.

Make sure to fill out the template with accurate information, and in the format requested.

  {% include alerts/important.html content="Please do not post multiple issues within the same report, as bugs can be either affecting different maintainers or some may be device specific where others are not" %}

  {%- capture content %}[Logcats]({{ "how-to/logcat/" | relative_url }}) *must* be attached for all Android bugs, and *must* be captured right after reproducing the issue.{% endcapture %}
  {% include alerts/important.html content=content %}

<a href="https://github.com/ItsVixano-releases/issues/issues/new?assignees=&labels=&projects=&template=android-issue.yml"><button class="btn btn-primary">Go to Android bugs</button></a>
