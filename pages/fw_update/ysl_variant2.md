---
sidebar: home_sidebar
title: Update firmware on ysl
folder: fw_update
permalink: /devices/ysl/fw_update/variant2/
device: ysl_variant2
---
{% assign device = site.data.devices[page.device] %}
{% capture path %}templates/device_specific/{{ device.firmware_update }}.md{% endcapture %}
{% include {{ path }} %}
