---
sidebar: home_sidebar
title: Update firmware on daisy
folder: fw_update
permalink: /devices/daisy/fw_update/
device: daisy
---
{% assign device = site.data.devices[page.device] %}
{% capture path %}templates/device_specific/{{ device.firmware_update }}.md{% endcapture %}
{% include {{ path }} %}
