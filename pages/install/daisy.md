---
sidebar: home_sidebar
title: Install LineageOS on daisy
folder: install
permalink: /devices/daisy/install/
device: daisy
---
{% include templates/device_install.md %}
