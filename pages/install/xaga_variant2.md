---
sidebar: home_sidebar
title: Install LineageOS on xaga
folder: install
permalink: /devices/xaga/install/variant2/
device: xaga_variant2
---
{% include templates/device_install.md %}
