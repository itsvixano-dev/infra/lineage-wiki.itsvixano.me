---
sidebar: home_sidebar
title: Install LineageOS on xaga
folder: install
permalink: /devices/xaga/install/variant3/
device: xaga_variant3
---
{% include templates/device_install.md %}
