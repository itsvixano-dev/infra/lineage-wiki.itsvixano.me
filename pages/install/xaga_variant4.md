---
sidebar: home_sidebar
title: Install LineageOS on xaga
folder: install
permalink: /devices/xaga/install/variant4/
device: xaga_variant4
---
{% include templates/device_install.md %}
