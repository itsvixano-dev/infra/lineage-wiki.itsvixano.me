---
sidebar: home_sidebar
title: Install LineageOS on sakura
folder: install
permalink: /devices/sakura/install/
device: sakura
---
{% include templates/device_install.md %}
