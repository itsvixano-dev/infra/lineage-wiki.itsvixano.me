---
sidebar: home_sidebar
title: Update LineageOS on xaga
folder: update
permalink: /devices/xaga/update/variant3/
device: xaga_variant3
---
{% include templates/device_update.md %}
