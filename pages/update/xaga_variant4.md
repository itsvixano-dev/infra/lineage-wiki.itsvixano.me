---
sidebar: home_sidebar
title: Update LineageOS on xaga
folder: update
permalink: /devices/xaga/update/variant4/
device: xaga_variant4
---
{% include templates/device_update.md %}
