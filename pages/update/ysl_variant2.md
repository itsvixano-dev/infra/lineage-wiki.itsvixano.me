---
sidebar: home_sidebar
title: Update LineageOS on ysl
folder: update
permalink: /devices/ysl/update/variant2/
device: ysl_variant2
---
{% include templates/device_update.md %}
