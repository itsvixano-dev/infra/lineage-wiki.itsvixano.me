---
sidebar: home_sidebar
title: Update LineageOS on xaga
folder: update
permalink: /devices/xaga/update/variant1/
device: xaga_variant1
---
{% include templates/device_update.md %}
