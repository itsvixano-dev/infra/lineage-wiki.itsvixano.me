---
sidebar: home_sidebar
title: Update LineageOS on ysl
folder: update
permalink: /devices/ysl/update/variant1/
device: ysl_variant1
---
{% include templates/device_update.md %}
