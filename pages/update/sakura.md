---
sidebar: home_sidebar
title: Update LineageOS on sakura
folder: update
permalink: /devices/sakura/update/
device: sakura
---
{% include templates/device_update.md %}
