---
sidebar: home_sidebar
title: Update LineageOS on daisy
folder: update
permalink: /devices/daisy/update/
device: daisy
---
{% include templates/device_update.md %}
