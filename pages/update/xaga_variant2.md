---
sidebar: home_sidebar
title: Update LineageOS on xaga
folder: update
permalink: /devices/xaga/update/variant2/
device: xaga_variant2
---
{% include templates/device_update.md %}
