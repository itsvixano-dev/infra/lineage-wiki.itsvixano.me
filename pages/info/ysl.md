---
sidebar: home_sidebar
title: Info about ysl variants
folder: info
permalink: /devices/ysl/
redirect_from:
- /devices/ysl/install
- /devices/ysl/update
device: ysl
toc: false
---
{% include templates/device_variants.md info=true %}
