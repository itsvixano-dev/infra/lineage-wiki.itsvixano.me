---
sidebar: home_sidebar
title: Info about xaga variants
folder: info
permalink: /devices/xaga/
redirect_from:
- /devices/xaga/install
- /devices/xaga/update
device: xaga
toc: false
---
{% include templates/device_variants.md info=true %}
