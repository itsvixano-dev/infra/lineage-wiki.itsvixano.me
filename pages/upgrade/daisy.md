---
sidebar: home_sidebar
title: Upgrade LineageOS on daisy
folder: upgrade
permalink: /devices/daisy/upgrade/
device: daisy
---
{% include templates/device_upgrade.md %}
