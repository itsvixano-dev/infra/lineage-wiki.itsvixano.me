---
sidebar: home_sidebar
title: Upgrade LineageOS on ysl
folder: upgrade
permalink: /devices/ysl/upgrade/variant1/
device: ysl_variant1
---
{% include templates/device_upgrade.md %}
