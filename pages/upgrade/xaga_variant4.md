---
sidebar: home_sidebar
title: Upgrade LineageOS on xaga
folder: upgrade
permalink: /devices/xaga/upgrade/variant4/
device: xaga_variant4
---
{% include templates/device_upgrade.md %}
