---
sidebar: home_sidebar
title: Upgrade LineageOS on xaga
folder: upgrade
permalink: /devices/xaga/upgrade/variant3/
device: xaga_variant3
---
{% include templates/device_upgrade.md %}
