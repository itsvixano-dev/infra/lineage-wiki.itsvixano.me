---
sidebar: home_sidebar
title: Upgrade LineageOS on xaga
folder: upgrade
permalink: /devices/xaga/upgrade/variant2/
device: xaga_variant2
---
{% include templates/device_upgrade.md %}
