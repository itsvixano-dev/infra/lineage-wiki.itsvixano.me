---
sidebar: home_sidebar
title: Upgrade LineageOS on sakura
folder: upgrade
permalink: /devices/sakura/upgrade/
device: sakura
---
{% include templates/device_upgrade.md %}
