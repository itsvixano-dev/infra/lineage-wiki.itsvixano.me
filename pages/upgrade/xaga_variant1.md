---
sidebar: home_sidebar
title: Upgrade LineageOS on xaga
folder: upgrade
permalink: /devices/xaga/upgrade/variant1/
device: xaga_variant1
---
{% include templates/device_upgrade.md %}
