---
sidebar: home_sidebar
title: Upgrade LineageOS on ysl
folder: upgrade
permalink: /devices/ysl/upgrade/variant2/
device: ysl_variant2
---
{% include templates/device_upgrade.md %}
