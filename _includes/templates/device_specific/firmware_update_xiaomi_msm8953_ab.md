{% include templates/device_specific/firmware_update_xiaomi_fastboot_payload.md content="
fastboot flash cmnlib64_ab cmnlib64.img
fastboot flash cmnlib_ab cmnlib.img
fastboot flash devcfg_ab devcfg.img
fastboot flash dsp_ab dsp.img
fastboot flash keymaster_ab keymaster.img
fastboot flash mdtp_ab mdtp.img
fastboot flash modem_ab modem.img
fastboot flash rpm_ab rpm.img
fastboot flash sbl1_ab sbl1.img
fastboot flash tz_ab tz.img
" %}
